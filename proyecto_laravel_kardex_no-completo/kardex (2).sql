-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-03-2020 a las 18:19:46
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kardex`
--
CREATE DATABASE IF NOT EXISTS `kardex` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `kardex`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_consultar` ()  BEGIN
SELECT e.fecha_registro, e.factura, e.precio,e.cantidad,e.total,s.fecha_registro_s,s.precio_s,s.cantidad_s,s.total_s
FROM kardex k
LEFT JOIN entrada e on e.id_entrada = k.id_entrada
LEFT JOIN salida s on s.id_salida = k.id_salida;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_user_competition` ()  BEGIN
SELECT u.usuario_id, u.nombres, u.apellidos, com.nombre AS competencia FROM usuarios u
INNER JOIN detalle_cp dcp ON dcp.us_id = u.usuario_id
INNER JOIN competencias com ON com.competencia_id = dcp.competencia_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_UC` (IN `pa_nombres` VARCHAR(20), IN `pa_apellidos` VARCHAR(30), IN `pa_movil` CHAR(9), IN `pa_correo` VARCHAR(90), IN `pa_nombre` VARCHAR(30), IN `pa_descripcion` VARCHAR(50))  BEGIN
DECLARE usuario INT;
DECLARE competencia INT;
INSERT INTO `usuarios`(`nombres`, `apellidos`, `movil`, `correo`) VALUES (pa_nombres,pa_apellidos,pa_movil,pa_correo);

SET usuario = LAST_INSERT_ID();

INSERT INTO `competencias`(`nombre`, `descripcion`) VALUES (pa_nombre,pa_descripcion);

SET competencia = LAST_INSERT_ID();

INSERT INTO `detalle_cp`(`us_id`, `competencia_id`) VALUES (usuario,competencia);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencias`
--

CREATE TABLE `competencias` (
  `competencia_id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `us_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `competencias`
--

INSERT INTO `competencias` (`competencia_id`, `nombre`, `descripcion`, `us_id`) VALUES
(1, 'Trabajo en equipo', 'Requiere trabajar en proyectos a varios empleados', 0),
(2, 'laksdj', 'alsdkj', 0),
(3, 'alsdjsakldjaldskj', 'alajsldkjaklsdjalkdjalksdj', 0),
(4, 'Hola', 'Mundo', 0),
(5, 'alsdjsakldjaldskj', 'alajsldkjaklsdjalkdjalksdj', 0),
(6, 'alsdjsakldjaldskj', 'asdljasdlkdjaslkdjasdlkjasdlk', 0),
(7, 'alsdjsakldjaldskj', 'dskljhadslkfhlfdjkahfdiulfhuihfklfhakljsdfhlhds', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cp`
--

CREATE TABLE `detalle_cp` (
  `id_detalle` int(11) NOT NULL,
  `us_id` int(11) NOT NULL,
  `competencia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_cp`
--

INSERT INTO `detalle_cp` (`id_detalle`, `us_id`, `competencia_id`) VALUES
(1, 1, 1),
(2, 5, 4),
(3, 6, 5),
(4, 7, 6),
(5, 8, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrada`
--

CREATE TABLE `entrada` (
  `id_entrada` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `factura` int(11) NOT NULL,
  `precio` float NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `entrada`
--

INSERT INTO `entrada` (`id_entrada`, `fecha_registro`, `factura`, `precio`, `cantidad`, `total`) VALUES
(1, '2020-02-18', 259656, 1, 10, 1000),
(2, '2020-02-12', 2285211, 500, 5, 1000),
(4, '2020-02-11', 4545, 500, 5, 1000),
(5, '2020-02-12', 0, 500, 5, 1000),
(6, '2020-02-04', 4574524, 500, 5, 1000),
(15, '2020-01-30', 121, 1, 1, 1),
(16, '2020-03-05', 754, 4, 4, 444);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kardex`
--

CREATE TABLE `kardex` (
  `id_producto` int(11) NOT NULL,
  `id_entrada` int(11) DEFAULT NULL,
  `id_salida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `kardex`
--

INSERT INTO `kardex` (`id_producto`, `id_entrada`, `id_salida`) VALUES
(0, 1, 1),
(0, 2, 2),
(0, NULL, 3),
(0, 4, NULL),
(0, 5, NULL),
(0, 5, 1),
(0, 2, NULL),
(0, 6, NULL),
(0, NULL, 2),
(0, 2, NULL),
(0, 2, NULL),
(0, 2, NULL),
(0, 16, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre_producto`) VALUES
(15, 'maquina de escribir'),
(21, 'Computadora'),
(22, 'Muebles'),
(23, 'Mesedora'),
(24, 'Carro de Bebe'),
(25, 'Trapeadores'),
(26, 'Lavadora'),
(27, 'Cocina'),
(28, 'fghfgh');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `nombre_rol` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `nombre_rol`) VALUES
(1, 'vcbfdg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salida`
--

CREATE TABLE `salida` (
  `id_salida` int(11) NOT NULL,
  `fecha_registro_s` date NOT NULL,
  `precio_s` int(11) NOT NULL,
  `cantidad_s` int(11) NOT NULL,
  `total_s` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `salida`
--

INSERT INTO `salida` (`id_salida`, `fecha_registro_s`, `precio_s`, `cantidad_s`, `total_s`) VALUES
(1, '2020-02-11', 500, 10, 1000),
(2, '2020-02-04', 500, 5, 1000),
(3, '2020-02-18', 500, 2000, 1000),
(10, '2020-01-31', 4, 3, 3),
(11, '2020-02-07', 3263, 151, 5456412);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sexo`
--

CREATE TABLE `sexo` (
  `id_sexo` int(11) NOT NULL,
  `nombre_sexo` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `direccion` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `clave` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario_id` int(11) NOT NULL,
  `nombres` varchar(20) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `movil` char(9) NOT NULL,
  `correo` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario_id`, `nombres`, `apellidos`, `movil`, `correo`) VALUES
(1, 'Pedro', 'Mendoza', '7730-1602', 'pedrux_mendoza@hotmail.com'),
(2, 'Michelle', 'De Mendoza', '1234-5678', 'michelle.demendoza@gmail.com'),
(3, 'Rocio', 'Miranda', '8888-8888', 'isidro@isidro.com'),
(4, 'Rocio', 'Miranda', '8888-8888', 'isidro@isidro.com'),
(5, 'Michelle', 'Giron', '1235-6789', 'pedroteamo@gmail.com'),
(6, 'Rocio', 'Miranda', '35241', 'asdasdsd@corre.com'),
(7, 'Rocio', 'Miranda', '8888-8888', 'isidro@isidro.com'),
(8, 'Rocio', 'Miranda', '8888-8888', 'asdasdsadsda@asldkj.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `competencias`
--
ALTER TABLE `competencias`
  ADD PRIMARY KEY (`competencia_id`);

--
-- Indices de la tabla `detalle_cp`
--
ALTER TABLE `detalle_cp`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `fk_competencias` (`competencia_id`),
  ADD KEY `fk_usuarios` (`us_id`);

--
-- Indices de la tabla `entrada`
--
ALTER TABLE `entrada`
  ADD PRIMARY KEY (`id_entrada`);

--
-- Indices de la tabla `kardex`
--
ALTER TABLE `kardex`
  ADD KEY `id_entrada` (`id_entrada`),
  ADD KEY `id_salida` (`id_salida`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `salida`
--
ALTER TABLE `salida`
  ADD PRIMARY KEY (`id_salida`);

--
-- Indices de la tabla `sexo`
--
ALTER TABLE `sexo`
  ADD PRIMARY KEY (`id_sexo`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_sexo` (`id_sexo`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `competencias`
--
ALTER TABLE `competencias`
  MODIFY `competencia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `detalle_cp`
--
ALTER TABLE `detalle_cp`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `entrada`
--
ALTER TABLE `entrada`
  MODIFY `id_entrada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `salida`
--
ALTER TABLE `salida`
  MODIFY `id_salida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `sexo`
--
ALTER TABLE `sexo`
  MODIFY `id_sexo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_cp`
--
ALTER TABLE `detalle_cp`
  ADD CONSTRAINT `fk_competencias` FOREIGN KEY (`competencia_id`) REFERENCES `competencias` (`competencia_id`),
  ADD CONSTRAINT `fk_usuarios` FOREIGN KEY (`us_id`) REFERENCES `usuarios` (`usuario_id`);

--
-- Filtros para la tabla `kardex`
--
ALTER TABLE `kardex`
  ADD CONSTRAINT `kardex_ibfk_1` FOREIGN KEY (`id_entrada`) REFERENCES `entrada` (`id_entrada`),
  ADD CONSTRAINT `kardex_ibfk_2` FOREIGN KEY (`id_salida`) REFERENCES `salida` (`id_salida`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_sexo`) REFERENCES `sexo` (`id_sexo`),
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
